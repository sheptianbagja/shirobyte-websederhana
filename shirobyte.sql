-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 25, 2019 at 07:54 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `shirobyte`
--

-- --------------------------------------------------------

--
-- Table structure for table `tabel_user`
--

CREATE TABLE `tabel_user` (
  `id` int(11) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `gender` varchar(100) NOT NULL,
  `no_telepon` varchar(100) NOT NULL,
  `pekerjaan` varchar(100) NOT NULL,
  `photo` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tabel_user`
--

INSERT INTO `tabel_user` (`id`, `email`, `password`, `nama`, `gender`, `no_telepon`, `pekerjaan`, `photo`) VALUES
(6, 'sheptianbagjautama@gmail.com', 'komputer', 'sheptian bagja utama', 'male', '087824392239', 'karyawan swasta', '1556171227292.jpg'),
(7, 'yuliani@gmail.com', 'komputer', 'yuliani utami putri', 'female', '087823942', 'belum bekerja', '1556171250713.jpg'),
(8, 'ranggajaya@gmail.com', 'komputer', 'rangga jaya utama', 'male', '0877234234', 'belum bekerja', '1556171285334.jpg'),
(9, 'rahma@gmail.com', '71431b1e88117facdc7584c476e09452', 'rahma aulia', 'female', '087823948', 'pegawai negeri', '1556171454963.jpg'),
(10, 'aldian@gmail.com', '751a39f76742f5cc1e4dafd351bda756', 'alvian ahja', 'male', '087823748232', 'pegawai negeri', '1556171604989.png'),
(11, 'opik@gmail.com', '751a39f76742f5cc1e4dafd351bda756', 'opik saepullah', 'male', '087238942374', 'belum bekerja', '1556171659330.png');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tabel_user`
--
ALTER TABLE `tabel_user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tabel_user`
--
ALTER TABLE `tabel_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
